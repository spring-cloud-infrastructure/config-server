# Config Server

Check out [documentation](https://spring.io/projects/spring-cloud-config) for more info about Spring Config Server.

## 1. Configuration

- [bootstrap.yml](./src/main/resources/bootstrap.yml) (application bootstrap only)
- [config-repo](https://gitlab.com/spring-cloud-infrastructure/config-repo/-/tree/master/config) (externalized
  configuration)

### Profiles

- `native` - Use config-repo from local GIT repository.
- `gitlab` - Download `config-repo` from [Gitlab](https://gitlab.com/spring-cloud-infrastructure/config-repo).

## 2. Encryption and decryption of properties

```shell script
curl \
    --location \
    --request POST 'https://10.0.0.1:80/config-server/encrypt' \
    --header 'Authorization: Bearer <JWT_TOKEN>'\
    --data-raw 'text to encrypt' \
    --insecure
```

```shell script
curl \
    --location \
    --request POST 'https://10.0.0.1:80/config-server/decrypt' \
    --header 'Authorization: Bearer <JWT_TOKEN>'\
    --data-raw 'encrypted text' \
    --insecure
```

## 3. Build & Run

Check out [docker](https://gitlab.com/spring-cloud-infrastructure/docker) project.
