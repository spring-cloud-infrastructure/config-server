package com.spring.cloud

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("native")
internal class ConfigServerApplicationTest {

    @Test
    fun contextLoads() {
        // this method is empty purposely regarding of testing of application context
    }
}
